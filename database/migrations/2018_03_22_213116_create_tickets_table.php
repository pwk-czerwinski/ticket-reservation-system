<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_stadium_place');
            $table->foreign('id_stadium_place')->references('id')->on('stadium_places');
            $table->unsignedInteger('id_reservation');
            $table->foreign('id_reservation')->references('id')->on('reservations');
            $table->unsignedInteger('id_event');
            $table->foreign('id_event')->references('id')->on('events');
            $table->integer('price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
